" elenapan @ githuib
" ------------------------------------------------------------------
" Scheme setup {{{
set background=dark
hi! clear

if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'neptune'

hi Normal              guifg=#ECEFF4
hi Cursor              guifg=#ECEFF4      guibg=#F06292
hi CursorLine          guifg=NONE         guibg=#2E394D     gui=NONE
hi MatchParen          guifg=#ECEFF4      guibg=NONE        gui=UNDERLINE
hi Pmenu               guifg=#ECEFF4      guibg=#2E394D
hi PmenuThumb          guibg=#ECEFF4
hi PmenuSBar           guibg=#4D5D80
hi PmenuSel            guifg=#4D5D80      guibg=#98DC9A
hi ColorColumn         guibg=#1F2633
hi SpellBad            guifg=#F06292     guibg=NONE  gui=UNDERLINE
hi SpellCap            guifg=#9ABEFF     guibg=NONE  gui=UNDERLINE
hi SpellRare           guifg=#FF8A65     guibg=NONE  gui=UNDERLINE
hi SpellLocal          guifg=#6C7AC6     guibg=NONE  gui=UNDERLINE
hi NonText             guifg=#1F2633
hi LineNr              guifg=#4D5D80     guibg=NONE  gui=BOLD
hi CursorLineNr        guifg=#ECEFF4     guibg=#1F2633     gui=BOLD
hi Visual              guifg=#2E394D     guibg=#BD9AFF
hi IncSearch           guifg=#2E394D     guibg=#6C7AC6    gui=NONE
hi Search              guifg=#2E394D     guibg=#63CFBD
hi SignColumn          guifg=#4D5D80     guibg=#1F2633  gui=NONE
hi VertSplit           guifg=#1F2633     guibg=#1F2633     gui=BOLD
hi TabLine             guifg=#4D5D80     guibg=#1F2633     gui=NONE
hi TabLineSel          guifg=#ECEFF4     guibg=#1F2633
hi Folded              guifg=#9ABEFF     guibg=#1F2633     gui=BOLD,italic
hi Conceal             guifg=#96DFD3     guibg=NONE
hi Directory           guifg=#ECEFF4     guibg=#1F2633     gui=NONE
hi Title               guifg=#FF8A65     guibg=NONE  gui=BOLD
hi ErrorMsg            guifg=#F06292     guibg=NONE     gui=BOLD
hi DiffAdd             guifg=#2E394D     guibg=#98DC9A
hi DiffChange          guifg=#2E394D     guibg=#FFEE58
hi DiffDelete          guifg=#2E394D     guibg=#F06292
hi DiffText            guifg=#2E394D     guibg=#FF8A65    gui=BOLD
hi User1               guifg=#F06292     guibg=#1F2633
hi User2               guifg=#98DC9A     guibg=#1F2633
hi User3               guifg=#9ABEFF     guibg=#1F2633
hi User4               guifg=#FFEE58     guibg=#1F2633
hi User5               guifg=#BD9AFF     guibg=#1F2633
hi User6               guifg=#96DFD3     guibg=#1F2633
hi User7               guifg=#ECEFF4     guibg=#1F2633
hi User8               guifg=#4D5D80     guibg=#1F2633
hi User9               guifg=#4D5D80     guibg=#BD9AFF
hi! link CursorColumn  CursorLine
hi! link WildMenu      Visual
hi! link FoldColumn    SignColumn
hi! link WarningMsg    ErrorMsg
hi! link MoreMsg       Title
hi! link Question      MoreMsg
hi! link ModeMsg       MoreMsg
hi! link TabLineFill   StatusLineNC
hi! link SpecialKey    NonText

" Statusline
hi StatusLine          guifg=#FFEE58     guibg=NONE     gui=NONE
hi StatusLineNC        guifg=#FFEE58     guibg=NONE     gui=BOLD
hi MyStatuslineDevIcon  guifg=#2E394D gui=ITALIC guibg=NONE
hi MyStatuslineDevIconBody guifg=#2E394D gui=NONE guibg=NONE
hi MyStatuslineGitInfoBody  guifg=#2E394D gui=NONE guibg=#FF8A65
hi MyStatuslineGitInfo guifg=#FF8A65 gui=NONE guibg=NONE
hi MyStatuslineModified guifg=#2E394D gui=NONE guibg=NONE
hi MyStatuslineFilesize guifg=#2E394D gui=NONE guibg=NONE
hi MyStatuslineFilesizeBody guifg=#ECEFF4 gui=NONE guibg=#2E394D
hi MyStatuslineFiletype guibg=#2E394D gui=NONE guifg=#1F2633
hi FiletypeBody guibg=#2E394D gui=NONE guifg=#ECEFF4
hi MyStatuslineFilename guifg=#2E394D gui=NONE guibg=NONE
hi MyStatuslineFilenameBody guifg=#ECEFF4 gui=BOLD guibg=#2E394D
hi MyStatuslinePercentage guifg=#1F2633 gui=NONE guibg=NONE
hi MyStatuslinePercentageBody guibg=#1F2633 gui=NONE guifg=#96DFD3
hi MyStatuslineLineCol guifg=#2E394D gui=BOLD guibg=NONE
hi MyStatuslineLineColBody guibg=#2E394D gui=NONE guifg=#ECEFF4
hi MyStatuslineSeparator guifg=#2E394D gui=NONE guibg=#2E394D
hi MyStatuslineSeparator2 guifg=#2E394D gui=NONE guibg=NONE
hi MyBuffer guifg=#96DFD3 gui=NONE guibg=#2E394D
hi MyBufferNum guifg=#ECEFF4 gui=NONE guibg=#2E394D
hi MySeparator guifg=#2E394D gui=NONE guibg=NONE
hi MyBatterySymbol guifg=#98DC9A gui=NONE guibg=#2E394D
hi MyBattery guifg=#ECEFF4 gui=NONE guibg=#2E394D
hi MyStatuslineModifiedBody guifg=#98DC9A gui=BOLD guibg=#2E394D

hi MyPercentage guifg=#ECEFF4 gui=NONE guibg=#2E394D

hi MyFirstDir guifg=#4D5D80 gui=BOLD guibg=#2E394D
hi MySecondDir guifg=#98DC9A gui=BOLD guibg=#2E394D
hi MyThirdDir guifg=#9ABEFF gui=BOLD guibg=#2E394D

hi MyGitBranch guifg=#98DC9A gui=BOLD guibg=#2E394D

hi MyGitSymbol guifg=#ECEFF4 gui=NONE guibg=#2E394D
hi MyGitNumber guifg=#ECEFF4 gui=NONE guibg=#2E394D

hi MyNerdTree guifg=#ECEFF4 gui=BOLD guibg=#2E394D
hi MyNerdTreeTree guifg=#2E394D gui=NONE guibg=#FF8A65
hi MyNerdTreeAccent guifg=#2E394D gui=NONE guibg=NONE
hi MyNerdTreeAccentBody guifg=#FF8A65 gui=NONE guibg=NONE

hi WebDevIconsDefaultFolderSymbol guifg=#FF8A65 gui=NONE guibg=NONE
hi WebDevIconsDefaultFolderClosedSymbol guifg=#ECEFF4 gui=NONE guibg=NONE
hi DevIconsDefaultFolderOpenSymbol guifg=#4D5D80 gui=NONE guibg=NONE
hi WebDevIconsUnicodeDecorateFolderNodesDefaultSymbol guifg=#ECEFF4 gui=NONE guibg=NONE
hi WebDevIconsNERDTreeDirOpen guifg=#ECEFF4 gui=NONE guibg=NONE 


hi NormalColor guibg=#96DFD3 gui=BOLD guifg=#2E394D
hi InsertColor guibg=#F06292 gui=BOLD guifg=#2E394D
hi ReplaceColor guibg=#FFEE58 gui=BOLD guifg=#2E394D
hi VisualColor guibg=#BD9AFF gui=BOLD guifg=#2E394D
hi CommandColor guibg=#96DFD3 gui=BOLD guifg=#2E394D

hi MystatuslineBattery guibg=#2E394D gui=BOLD guifg=#F06292

hi ScrollView guifg=#4D5D80 gui=NONE guibg=#4D5D80

hi MyCurrentFolder guifg=#98DC9A gui=ITALIC guibg=#2E394D
hi MyFileFormat guifg=#ECEFF4 gui=NONE guibg=#2E394D

hi Linter guifg=#9ABEFF gui=BOLD guibg=#2E394D
hi LinterOk guifg=#98DC9A gui=BOLD guibg=#2E394D
hi LinterNonErrors guifg=#FF8A65 gui=BOLD guibg=#2E394D
hi LinterErrors guifg=#F06292 gui=BOLD guibg=#2E394D

" COC
hi CocGitAddedSign guifg=#98DC9A gui=BOLD guibg=NONE
hi CocGitChangeRemovedSign guifg=#FFEE58 gui=BOLD guibg=NONE
hi CocGitChangedSign guifg=#FFEE58 gui=BOLD guibg=NONE
hi CocGitRemovedSign guifg=#F06292 gui=BOLD guibg=NONE
hi CocGitTopRemovedSign guifg=#F06292 gui=BOLD guibg=NONE

" Signify
hi SignifySignAdd    guifg=#98DC9A gui=BOLD guibg=NONE
hi SignifySignDelete guifg=#F06292 gui=BOLD guibg=NONE
hi SignifySignChange    guifg=#FFEE58 gui=BOLD guibg=NONE

" NERDTree-Git-Plugin
hi NERDTreeGitStatusModified guifg=#FF8A65 gui=NONE guibg=NONE 

" Dashboard
hi DashboardCenter guifg=#96DFD3 gui=BOLD guibg=NONE
hi DashboardShortcut guifg=#F06292 gui=BOLD guibg=NONE
hi DashboardFooter guifg=#9ABEFF gui=NONE guibg=NONE
hi DashboardHeader guifg=#98DC9A gui=NONE guibg=NONE

" ALE
hi ALEErrorSign guifg=#F06292 gui=NONE guibg=#1F2633
hi ALEWarningSign guifg=#F06292 gui=NONE guibg=#1F2633

"}
" Generic syntax {{{
hi Delimiter       guifg=#9ABEFF   gui=NONE
hi Comment         guifg=#4D5D80   gui=BOLD,italic
hi Underlined      guifg=#98DC9A   gui=UNDERLINE
hi Type            guifg=#ECEFF4   gui=NONE
hi String          guifg=#98DC9A   gui=NONE
hi Keyword         guifg=#9ABEFF   gui=NONE
hi Todo            guifg=#FF8A65   guibg=NONE     gui=BOLD,underline
hi Urgent          guifg=#F06292   guibg=NONE     gui=BOLD,underline
hi Done            guifg=#9ABEFF   guibg=NONE     gui=BOLD,underline
hi Function        guifg=#96DFD3   gui=NONE
hi Identifier      guifg=#ECEFF4   gui=NONE
hi Statement       guifg=#9ABEFF   gui=BOLD
hi Constant        guifg=#FF8A65   gui=BOLD
hi Number          guifg=#98DC9A   gui=NONE
hi Boolean         guifg=#98DC9A   gui=NONE
hi Special         guifg=#96DFD3   gui=BOLD
hi Ignore          guifg=#2E394D   gui=NONE
hi PreProc         guifg=#9ABEFF   gui=NONE
hi Operator        guifg=#96DFD3   gui=BOLD
" hi! link Operator  Delimiter
hi! link Error     ErrorMsg

"}}}
" HTML {{{
hi htmlTagName              guifg=#9ABEFF gui=BOLD
hi htmlTag                  guifg=#9ABEFF
hi htmlArg                  guifg=#96DFD3
hi htmlH1                   gui=BOLD
hi htmlBold                 gui=BOLD
hi htmlItalic               gui=UNDERLINE
hi htmlUnderline            gui=UNDERLINE
hi htmlBoldItalic           gui=BOLD,underline
hi htmlBoldUnderline        gui=BOLD,underline
hi htmlUnderlineItalic      gui=UNDERLINE
hi htmlBoldUnderlineItalic  gui=BOLD,underline
hi htmlLink                 guifg=#ECEFF4 gui=BOLD,underline
hi! link htmlEndTag         htmlTag
hi htmlTitle guifg=#ECEFF4 gui=BOLD,underline
hi htmlTagN                 guifg=#ECEFF4 gui=BOLD
hi htmlString               guifg=#9CCC65
hi htmlComment              guifg=#4D5D80 gui=BOLD,italic

" CSS
hi cssClassName guifg=#98DC9A
hi cssIdentifier guifg=#98DC9A
hi cssValueNumber guifg=#F06292
hi! link cssValueLength cssValueNumber
hi cssPseudoClassId guifg=#98DC9A gui=BOLD

"}}}
" XML {{{
hi xmlTagName       guifg=#98DC9A
hi xmlTag           guifg=#9CCC65
hi! link xmlString  xmlTagName
hi! link xmlAttrib  xmlTag
hi! link xmlEndTag  xmlTag
hi! link xmlEqual   xmlTag

"}}}
" JavaScript {{{
hi! link javaScript        Normal
hi! link javaScriptBraces  Delimiter

"}}}
" PHP {{{
hi phpSpecialFunction    guifg=#BD9AFF
hi phpIdentifier         guifg=#FF8A65
hi phpParent             guifg=#2E394D
hi! link phpVarSelector  phpIdentifier
hi! link phpHereDoc      String
hi! link phpDefine       Statement

"}}}
" Markdown {{{
hi markdownHeadingRule          guifg=#FFEE58
hi! link markdownHeadingDelimiter   markdownHeadingRule
hi! link markdownLinkDelimiter      Delimiter
hi! link markdownURLDelimiter       Delimiter
hi! link markdownCodeDelimiter      NonText
hi markdownLinkDelimiter    guifg=#4D5D80 guibg=NONE gui=NONE
hi! link markdownLinkTextDelimiter  markdownLinkDelimiter
hi markdownLinkText         guifg=#98DC9A guibg=NONE gui=BOLD,underline
hi! link markdownUrl                markdownLinkText
hi! link markdownUrlTitleDelimiter  markdownLinkText
hi! link markdownAutomaticLink      markdownLinkText
hi! link markdownIdDeclaration      markdownLinkText
hi markdownCode                     guifg=#9ABEFF guibg=NONE gui=NONE
hi! link markdownCodeBlock          String
hi! link markdownCodeBlock markdownCode
hi! link markdownCodeDelimiter markdownCode
hi markdownBold                     guifg=#BD9AFF guibg=NONE gui=BOLD
hi markdownItalic                   guifg=#BD9AFF guibg=NONE gui=ITALIC
hi markdownBlockquote               guifg=#4D5D80 guibg=NONE gui=ITALIC,bold
hi markdownRule                     guifg=#4D5D80 guibg=NONE gui=ITALIC,bold

hi markdownH1 guifg=#FFEE58 guibg=NONE gui=BOLD
hi markdownH2 guifg=#FFEE58 guibg=NONE gui=BOLD
hi markdownH3 guifg=#98DC9A guibg=NONE gui=BOLD
hi markdownH4 guifg=#98DC9A guibg=NONE gui=BOLD
hi markdownH5 guifg=#98DC9A guibg=NONE gui=NONE
hi markdownH6 guifg=#98DC9A guibg=NONE gui=NONE

hi markdownListMarker guifg=#96DFD3 guibg=NONE gui=BOLD
hi markdownOrderedListMarker guifg=#FFEE58 guibg=NONE gui=BOLD

"}}}
" Ruby {{{
hi! link rubyDefine                 Statement
hi! link rubyLocalVariableOrMethod  Identifier
hi! link rubyConstant               Constant
hi! link rubyInstanceVariable       Number
hi! link rubyStringDelimiter        rubyString

"}}}
" Git {{{
hi gitCommitBranch               guifg=#FFEE58
hi gitCommitSelectedType         guifg=#9ABEFF
hi gitCommitSelectedFile         guifg=#9ABEFF
hi gitCommitUnmergedType         guifg=#F06292
hi gitCommitUnmergedFile         guifg=#F06292
hi! link gitCommitFile           Directory
hi! link gitCommitUntrackedFile  gitCommitUnmergedFile
hi! link gitCommitDiscardedType  gitCommitUnmergedType
hi! link gitCommitDiscardedFile  gitCommitUnmergedFile

"}}}
" Vim {{{
hi! link vimSetSep    Delimiter
hi! link vimContinue  Delimiter
hi! link vimHiAttrib  Constant
hi! link vimFunc      Function
hi! link vimFunction  Function
hi! link vimUserFunc  Function


"}}}
" LESS {{{
hi lessVariable             guifg=#FF8A65
hi! link lessVariableValue  Normal

"}}}
" NERDTree {{{
hi! link NERDTreeHelp      Comment
hi! link NERDTreeExecFile  String
hi NERDTreeOpenable guibg=#1F2633 gui=NONE guifg=#ECEFF4
hi NERDTreeClosable guibg=#1F2633 gui=NONE guifg=#FF8A65
hi NERDTreeCWD guifg=#1F2633 guibg=NONE gui=BOLD
hi NERDTreeDirSlash guifg=#1F2633 guibg=NONE gui=NONE

"}}}
" Vimwiki {{{
hi! link VimwikiBold markdownBold
hi! link VimwikiItalic markdownItalic
hi! link VimwikiBoldChar markdownBold
hi! link VimwikiItalicChar markdownItalic
hi! link VimwikiBoldCharT   VimwikiBoldChar
hi! link VimwikiItalicCharT   VimwikiItalicChar
hi VimwikiBoldItalicChar guifg=#96DFD3 guibg=NONE gui=ITALIC,bold
hi! link VimwikiItalicBoldChar VimwikiBoldItalicChar
hi! link VimwikiBoldItalicCharT VimwikiBoldItalicChar
hi! link VimwikiItalicBoldCharT VimwikiBoldItalicChar
" VimwikiEqInChar xxx links to VimwikiMarkers
" VimwikiDelTextChar xxx links to VimwikiMarkers
" VimwikiEqInCharT xxx links to VimwikiMarkers
" VimwikiCodeCharT xxx links to VimwikiMarkers
" VimwikiDelTextCharT xxx links to VimwikiMarkers
hi! link VimwikiHeaderChar  markdownHeadingDelimiter
hi! link VimwikiList        markdownListMarker
hi! link VimwikiCode        markdownCode
hi! link VimwikiCodeChar    markdownCodeDelimiter
hi! link VimwikiHeader1     markdownH1
hi! link VimwikiHeader2     markdownH2
hi! link VimwikiHeader3     markdownH3
hi! link VimwikiHeader4     markdownH4
hi! link VimwikiHeader5     markdownH5
hi! link VimwikiHeader6     markdownH6

"}}}
" Help {{{
hi! link helpExample         String
hi! link helpHeadline        Title
hi! link helpSectionDelim    Comment
hi! link helpHyperTextEntry  Statement
hi! link helpHyperTextJump   Underlined
hi! link helpURL             Underlined

"}}}
" CtrlP {{{
hi CtrlPMatch   guifg=#F06292   gui=BOLD
hi CtrlPLinePre guifg=#96DFD3 gui=BOLD

"}}}
" Mustache {{{
hi mustacheSection           guifg=#63CFBD  gui=BOLD
hi mustacheMarker            guifg=#96DFD3
hi mustacheVariable          guifg=#63CFBD
hi mustacheVariableUnescape  guifg=#F06292
hi mustachePartial           guifg=#6C7AC6

"}}}
" Shell {{{
hi shDerefSimple     guifg=#FF8A65
hi! link shDerefVar  shDerefSimple

"}}}
" Syntastic {{{
hi SyntasticWarningSign       guifg=#FFEE58  guibg=NONE
hi SyntasticErrorSign         guifg=#F06292  guibg=NONE
hi SyntasticStyleWarningSign  guifg=#9ABEFF  guibg=NONE
hi SyntasticStyleErrorSign    guifg=#98DC9A  guibg=NONE

"}}}
" Netrw {{{
hi netrwExe       guifg=#F06292
hi netrwClassify  guifg=#2E394D  gui=BOLD

"}}}
" Ledger {{{
hi ledgerAccount  guifg=#FF8A65
hi! link ledgerMetadata  Comment
hi! link ledgerTransactionStatus  Statement

"}}}
" Diff {{{
hi diffAdded  guifg=#9ABEFF
hi diffRemoved  guifg=#F06292
hi! link diffFile  PreProc
hi! link diffLine  Title

"}}}
" Plug {{{
hi plugSha  guifg=#FFEE58

"}}}
" Blade {{{
hi! link bladeStructure  PreProc
hi! link bladeParen      phpParent
hi! link bladeEchoDelim  PreProc

"}}}
