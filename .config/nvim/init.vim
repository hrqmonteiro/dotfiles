" ███    ██ ██    ██ ██ ███    ███ 
" ████   ██ ██    ██ ██ ████  ████ 
" ██ ██  ██ ██    ██ ██ ██ ████ ██ 
" ██  ██ ██  ██  ██  ██ ██  ██  ██ 
" ██   ████   ████   ██ ██      ██ 

scriptencoding utf8

" Plugins
call plug#begin('~/.config/nvim/plugged')
    Plug 'arcticicestudio/nord-vim'
    Plug 'cespare/vim-toml'
    Plug 'dense-analysis/ale'
    Plug 'hrqmonteiro/vim-devicons'
    Plug 'hrqmonteiro/vim-nerdtree-syntax-highlight'
    Plug 'itchyny/vim-gitbranch'
"    Plug 'itchyny/lightline.vim'
    Plug 'junegunn/vim-emoji'         
    Plug 'mattn/emmet-vim'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'preservim/nerdtree'
"    Plug 'skywind3000/vim-keysound
    Plug 'arcticicestudio/nord-vim'
    Plug 'toniz4/vim-stt'
    Plug 'tpope/vim-commentary'
    Plug 'Valloric/MatchTagAlways'
    Plug 'Yggdroot/indentLine'
call plug#end()

" Source files
source $HOME/.config/nvim/plug-config/coc.vim
source $HOME/.config/nvim/statusline/statusline.vim

" Statusline functions
" augroup Statusline
"     autocmd VimEnter * call Battery(0)
"     autocmd VimEnter * call BatterySymbol(0)
"     autocmd VimEnter * call GitCommitSymbol(0)
"     autocmd VimEnter * call GitCommitNumber(0)
"     autocmd VimEnter * call FileDir(0)
"     autocmd VimEnter * call GitUrl(0)
" augroup end

let g:nord_cursor_line_number_background = 1
let g:nord_uniform_status_lines = 1
let g:nord_bold_vertical_split_line = 1
let g:nord_uniform_diff_background = 1
let g:nord_bold = 1
let g:nord_italic = 1
let g:nord_italic_comments = 1
let g:nord_underline = 1

" Configs
colorscheme neptune
filetype off
filetype plugin indent on
let mapleader=','
set autochdir
set clipboard=unnamedplus
set cmdheight=1
set cursorline
set expandtab
set fillchars=vert::
set guifont=PragmataPro\ Nerd\ Font\
set incsearch
set hidden
set laststatus=2
set linespace=0
set mouse=nv
set nobackup
set noshowmode
set noswapfile
set number norelativenumber
set path+=*
set shiftwidth=2
set shortmess+=c
set signcolumn=yes
set smarttab
set splitbelow splitright
set t_Co=256
set tabstop=2
set termguicolors
set wildmenu
syntax on

" ALE
let g:ale_set_signs             = 1
let g:ale_sign_column_always    = 1
let g:ale_sign_error            = '> '
let g:ale_sign_warning          = '! '

let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\}

let g:ale_linters = {
\   'javascript': ['eslint'],
\   'css': ['csslint'],
\   'vim': ['vint'],
\}

" DevIcons
let g:webdevicons_enable_nerdtree = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsDefaultFolderOpenSymbol = ''
let g:WebDevIconsUnicodeDecorateFolderNodesDefaultSymbol = ''
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ''
let g:WebDevIconsUnicodeDecorateFolderNodes = v:true

let s:grey = '4D5D80'
let s:orange = 'FF8A65'

let g:DevIconsEnableFoldersOpenClose = 1
let g:WebDevIconsDefaultFolderSymbolColor = s:grey
let g:WebDevIconsDefaultOpenFolderSymbolColor= s:orange

" Emmet
let g:user_emmet_mode='a'
let g:user_emmet_leader_key='<C-X>'

" IndentLine
let g:indentLine_color_gui = '#4D5D80'
let g:indentLine_char = '│'
let g:indentLine_fileTypeExclude = [ 'dashboard', 'fzf', 'coc-explorer' ]
let g:indentLine_bufTypeExclude = [ 'help' ]

" NERDTree
" let g:NERDTreeDirArrowExpandable = '›'
" let g:NERDTreeDirArrowCollapsible = ''
" let g:NERDTreeDirArrowExpandable = ''
" let g:NERDTreeDirArrowCollapsible = ''
let g:NERDTreeDirArrowExpandable = ''
let g:NERDTreeDirArrowCollapsible = ''
let g:NERDTreeShowLineNumbers = 0
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 0
let g:NERDTreeWinSize = 32
let g:NERDTreeStatusline = '%#MyNerdTreeAccentBody#%#MyNerdTreeTree#🌲 %#MyNerdTree# %.25{getcwd()} %#MyNerdTreeAccent#'
" let g:NERDTreeStatusline = '%#NonText#'
let g:NERDTreeLimitedSyntax = 1
let g:NERDTreeHighlightCursorline = 0
let NERDTreeChDirMode=2

" STT
let g:stt_auto_insert=1
let g:stt_auto_quit=1

" Keybindings
nnoremap <silent> <C-N> :NERDTreeToggle<CR> 
nnoremap <silent> <leader>f :NERDTreeFind<CR>
nnoremap <leader>n :NERDTree ~/<CR>
nnoremap <silent> <C-M> :set relativenumber!<CR>
nnoremap <silent> <C-T> :ToggleTerm<CR>
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
