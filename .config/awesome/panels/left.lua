local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")
local icons = require

local keys = require("keys")

local panel = function(s)

local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local update_taglist = function (item, tag, index)
    if tag.selected then
        item.image = beautiful.taglist_icons_focused[index]
    elseif tag.urgent then
        item.image = beautiful.taglist_icons_urgent[index]
    elseif #tag:clients() > 0 then
        item.image = beautiful.taglist_icons_occupied[index]
    else
        item.image = beautiful.taglist_icons_empty[index]
    end
end

awful.screen.connect_for_each_screen(function(s)
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        layout = wibox.layout.fixed.vertical,
        widget_template = {
            widget = wibox.widget.imagebox,
            -- Add support for hover colors and an index label
            create_callback = function(self, tag, index, objects)
                update_taglist(self, tag, index)
            end,
            update_callback = function(self, tag, index, objects)
                update_taglist(self, tag, index)
            end,
        },
        buttons = taglist_buttons
    }
end)

    s.mytasklist = awful.widget.tasklist {
    screen   = s,
    filter   = awful.widget.tasklist.filter.currenttags,
    buttons  = tasklist_buttons,
    layout   = {
        spacing_widget = {
            {
                forced_width  = 24,
                forced_height = 5,
                thickness     = 1,
                color         = '#777777',
                widget        = wibox.widget.separator
            },
            valign = 'center',
            halign = 'center',
            widget = wibox.container.place,
        },
        spacing = 1,
        layout  = wibox.layout.fixed.vertical
    },
    -- Notice that there is *NO* wibox.wibox prefix, it is a template,
    -- not a widget instance.
    widget_template = {
        {
            wibox.widget.base.make_widget(),
            forced_height = 5,
            id            = 'background_role',
            widget        = wibox.container.background,
        },
        {
            awful.widget.clienticon,
            margins = 5,
            widget  = wibox.container.margin
        },
        nil,
        layout = wibox.layout.align.horizontal,
    },
}

s.mysystray = wibox.widget.systray({
  })

s.mysystray:set_horizontal(false)
s.mysystray:set_base_size(24)

    -- Create the wibox
    s.mywibox = awful.wibar({
      position = "left",
      screen = s,
      width = dpi(50),
    })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.vertical,
        {
          layout = wibox.layout.fixed.vertical,
          wibox.layout.margin(s.mytaglist, dpi(2), dpi(4), dpi(4), dpi(4)),
        },
        s.mytasklist,
        {
        layout = wibox.layout.fixed.vertical,
        wibox.layout.margin(s.mysystray, dpi(10), dpi(10), dpi(10), dpi(10)),
        halign = "center",
        }
    }

end

return panel
